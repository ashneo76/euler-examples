#!/usr/bin/env bash

set -euo pipefail

usage() {
    cat >&2 <<EOF
USAGE: ${BASH_SOURCE[0]} [test-number]

Example:
    ${BASH_SOURCE[0]} 016
EOF
}

run_test() {
    local -r TEST_FILE_NAME=${1}
    local -r TEST_BINARY=${2}

    rustc ${TEST_FILE_NAME} --test -o ${TEST_BINARY}
    ./${TEST_BINARY}
}

if [[ $# == 0 || "${1}" == "-h" ]]; then
    usage
    exit 1
fi

readonly PROBLEM_NUMBER=${1}
readonly PREFIX=prob
readonly BUILD_DIR=build
readonly FILE_NAME=solutions/${PREFIX}-${PROBLEM_NUMBER}.rs
readonly BINARY=${BUILD_DIR}/${PREFIX}-${PROBLEM_NUMBER}

mkdir -p ${BUILD_DIR}
run_test ${FILE_NAME} ${BINARY}
